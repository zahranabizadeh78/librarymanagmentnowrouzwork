﻿namespace MyProjectNorooz
{
    partial class RecordBookInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Nametxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Authoretxt = new System.Windows.Forms.TextBox();
            this.Publishertxt = new System.Windows.Forms.TextBox();
            this.Datetxt = new System.Windows.Forms.TextBox();
            this.Subtxt = new System.Windows.Forms.TextBox();
            this.Reviewtxt = new System.Windows.Forms.TextBox();
            this.Scoretxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Summerytxt = new System.Windows.Forms.TextBox();
            this.SaveInformation = new System.Windows.Forms.Button();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.OpenSerachForm = new System.Windows.Forms.Button();
            this.UserWellcome = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Authore :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Publisher :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 153);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "PublishDate :";
            // 
            // Nametxt
            // 
            this.Nametxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Nametxt.Location = new System.Drawing.Point(110, 51);
            this.Nametxt.Name = "Nametxt";
            this.Nametxt.Size = new System.Drawing.Size(138, 20);
            this.Nametxt.TabIndex = 4;
            this.Nametxt.TextChanged += new System.EventHandler(this.Nametxt_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Review :";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Subject :";
            // 
            // Authoretxt
            // 
            this.Authoretxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Authoretxt.Location = new System.Drawing.Point(110, 86);
            this.Authoretxt.Name = "Authoretxt";
            this.Authoretxt.Size = new System.Drawing.Size(138, 20);
            this.Authoretxt.TabIndex = 8;
            // 
            // Publishertxt
            // 
            this.Publishertxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Publishertxt.Location = new System.Drawing.Point(110, 121);
            this.Publishertxt.Name = "Publishertxt";
            this.Publishertxt.Size = new System.Drawing.Size(138, 20);
            this.Publishertxt.TabIndex = 9;
            // 
            // Datetxt
            // 
            this.Datetxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Datetxt.Location = new System.Drawing.Point(110, 153);
            this.Datetxt.Name = "Datetxt";
            this.Datetxt.Size = new System.Drawing.Size(138, 20);
            this.Datetxt.TabIndex = 10;
            // 
            // Subtxt
            // 
            this.Subtxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Subtxt.Location = new System.Drawing.Point(110, 188);
            this.Subtxt.Name = "Subtxt";
            this.Subtxt.Size = new System.Drawing.Size(138, 20);
            this.Subtxt.TabIndex = 11;
            // 
            // Reviewtxt
            // 
            this.Reviewtxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Reviewtxt.Location = new System.Drawing.Point(110, 223);
            this.Reviewtxt.Name = "Reviewtxt";
            this.Reviewtxt.Size = new System.Drawing.Size(138, 20);
            this.Reviewtxt.TabIndex = 12;
            // 
            // Scoretxt
            // 
            this.Scoretxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Scoretxt.Location = new System.Drawing.Point(110, 259);
            this.Scoretxt.Name = "Scoretxt";
            this.Scoretxt.Size = new System.Drawing.Size(138, 20);
            this.Scoretxt.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 259);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Score :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(29, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Summery :";
            // 
            // Summerytxt
            // 
            this.Summerytxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Summerytxt.Location = new System.Drawing.Point(110, 292);
            this.Summerytxt.Name = "Summerytxt";
            this.Summerytxt.Size = new System.Drawing.Size(138, 20);
            this.Summerytxt.TabIndex = 17;
            // 
            // SaveInformation
            // 
            this.SaveInformation.Location = new System.Drawing.Point(32, 328);
            this.SaveInformation.Name = "SaveInformation";
            this.SaveInformation.Size = new System.Drawing.Size(216, 48);
            this.SaveInformation.TabIndex = 18;
            this.SaveInformation.Text = "Save Information";
            this.SaveInformation.UseVisualStyleBackColor = true;
            this.SaveInformation.Click += new System.EventHandler(this.SaveInformation_Click);
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Location = new System.Drawing.Point(272, 20);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.Size = new System.Drawing.Size(843, 410);
            this.dataGridViewMain.TabIndex = 19;
            // 
            // OpenSerachForm
            // 
            this.OpenSerachForm.Location = new System.Drawing.Point(32, 382);
            this.OpenSerachForm.Name = "OpenSerachForm";
            this.OpenSerachForm.Size = new System.Drawing.Size(216, 48);
            this.OpenSerachForm.TabIndex = 20;
            this.OpenSerachForm.Text = "Search Book";
            this.OpenSerachForm.UseVisualStyleBackColor = true;
            this.OpenSerachForm.Click += new System.EventHandler(this.OpenSerachForm_Click);
            // 
            // UserWellcome
            // 
            this.UserWellcome.AutoSize = true;
            this.UserWellcome.Location = new System.Drawing.Point(29, 17);
            this.UserWellcome.Name = "UserWellcome";
            this.UserWellcome.Size = new System.Drawing.Size(41, 13);
            this.UserWellcome.TabIndex = 21;
            this.UserWellcome.Text = "Name :";
            // 
            // RecordBookInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1148, 448);
            this.Controls.Add(this.UserWellcome);
            this.Controls.Add(this.OpenSerachForm);
            this.Controls.Add(this.dataGridViewMain);
            this.Controls.Add(this.SaveInformation);
            this.Controls.Add(this.Summerytxt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Scoretxt);
            this.Controls.Add(this.Reviewtxt);
            this.Controls.Add(this.Subtxt);
            this.Controls.Add(this.Datetxt);
            this.Controls.Add(this.Publishertxt);
            this.Controls.Add(this.Authoretxt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Nametxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "RecordBookInformation";
            this.Text = "Library Managment";
            this.Load += new System.EventHandler(this.RecordBookInformation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Nametxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Authoretxt;
        private System.Windows.Forms.TextBox Publishertxt;
        private System.Windows.Forms.TextBox Datetxt;
        private System.Windows.Forms.TextBox Subtxt;
        private System.Windows.Forms.TextBox Reviewtxt;
        private System.Windows.Forms.TextBox Scoretxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Summerytxt;
        private System.Windows.Forms.Button SaveInformation;
        private System.Windows.Forms.DataGridView dataGridViewMain;
        private System.Windows.Forms.Button OpenSerachForm;
        private System.Windows.Forms.Label UserWellcome;
    }
}