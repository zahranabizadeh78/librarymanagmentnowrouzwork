﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace MyProjectNorooz
{
    public partial class RecordBookInformation : Form
    {
        public string UserName { get; set; }
        public RecordBookInformation(string name)
        {
            UserName = name;
            InitializeComponent();
        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }

        private void Nametxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void BookPicture_Click(object sender, EventArgs e)
        {

        }

        private void RecordBookInformation_Load(object sender, EventArgs e)
        {
            Nametxt.Text = "";
            Authoretxt.Text = "";
            Publishertxt.Text = "";
            Summerytxt.Text = "";
            Reviewtxt.Text = "";
            Datetxt.Text = "";
            Subtxt.Text = "";
            Scoretxt.Text = "";

            UserWellcome.Text = UserName + " Wellcome To System ...!";


           
            DataTable BookInf = new DataTable();
            BookInf.Columns.Add("Name", typeof(string));
            BookInf.Columns.Add("Auther", typeof(string));
            BookInf.Columns.Add("publisher", typeof(string));
            BookInf.Columns.Add("publishDate", typeof(string));
            BookInf.Columns.Add("Subject", typeof(string));
            BookInf.Columns.Add("Riview", typeof(string));
            BookInf.Columns.Add("Score", typeof(string));
            BookInf.Columns.Add("Summery", typeof(string));

            List<string> AllBookInformations = new List<string>();
            if (File.Exists("F:\\SaveBook.txt"))
            {
                StreamReader SaveBook = new StreamReader("F:\\SaveBook.txt");
                while (!SaveBook.EndOfStream)
                {
                    AllBookInformations.Add(SaveBook.ReadLine());
                }
                SaveBook.Close();

            }

            foreach (var Item in AllBookInformations)
            {
                DataRow row = BookInf.NewRow();
                row["Name"] = Item.Split('/')[0];
                row["Auther"] = Item.Split('/')[1];
                row["Publisher"] = Item.Split('/')[2];
                row["PublishDate"] = Item.Split('/')[3];
                row["Subject"] = Item.Split('/')[4];
                row["Riview"] = Item.Split('/')[5];
                row["Score"] = Item.Split('/')[6];
                row["Summery"] = Item.Split('/')[7];
                BookInf.Rows.Add(row);
            }

            dataGridViewMain.DataSource = BookInf;

        }

        private void SaveInformation_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(Nametxt.Text) || string.IsNullOrEmpty(Authoretxt.Text))
            {
                MessageBox.Show("Please at least enter Name and Authore !!!" , "Error!");
                return;
            }
            string BookInform = Nametxt.Text + "/" + Authoretxt.Text + "/" + Publishertxt.Text + "/" + Datetxt.Text + "/" + Subtxt.Text + "/" + Reviewtxt.Text + "/" + Scoretxt.Text + "/" + Summerytxt.Text;

            StreamWriter Save = new StreamWriter("F:\\SaveBook.txt", true);
            Save.WriteLine(BookInform);
            Save.Close();

            Nametxt.Text = "";
            Authoretxt.Text = "";
            Publishertxt.Text = "";
            Summerytxt.Text = "";
            Reviewtxt.Text = "";
            Datetxt.Text = "";
            Subtxt.Text = "";
            Scoretxt.Text = "";

            List<string> AllBookInformations = new List<string>();
            if (File.Exists("F:\\SaveBook.txt"))
            {
                StreamReader SaveBook = new StreamReader("F:\\SaveBook.txt");
                while (!SaveBook.EndOfStream)
                {
                    AllBookInformations.Add(SaveBook.ReadLine());
                }
                SaveBook.Close();

            }

            DataTable BookInf = new DataTable();
            BookInf.Columns.Add("Name", typeof(string));
            BookInf.Columns.Add("Auther", typeof(string));
            BookInf.Columns.Add("publisher", typeof(string));
            BookInf.Columns.Add("publishDate", typeof(string));
            BookInf.Columns.Add("Subject", typeof(string));
            BookInf.Columns.Add("Riview", typeof(string));
            BookInf.Columns.Add("Score", typeof(string));
            BookInf.Columns.Add("Summery", typeof(string));

            foreach (var Item in AllBookInformations)
            {
                DataRow row = BookInf.NewRow();
                row["Name"] = Item.Split('/')[0];
                row["Auther"] = Item.Split('/')[1];
                row["Publisher"] = Item.Split('/')[2];
                row["PublishDate"] = Item.Split('/')[3];
                row["Subject"] = Item.Split('/')[4];
                row["Riview"] = Item.Split('/')[5];
                row["Score"] = Item.Split('/')[6];
                row["Summery"] = Item.Split('/')[7];
                BookInf.Rows.Add(row);
            }

            dataGridViewMain.DataSource = BookInf;
        }

        private void OpenSerachForm_Click(object sender, EventArgs e)
        {
            Search searchForm = new Search();
            searchForm.Show();
        }
    }
}

