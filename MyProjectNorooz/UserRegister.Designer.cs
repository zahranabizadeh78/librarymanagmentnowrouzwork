﻿namespace MyProjectNorooz
{
    partial class UserRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Save = new System.Windows.Forms.Button();
            this.Return = new System.Windows.Forms.Button();
            this.Nametxt1 = new System.Windows.Forms.TextBox();
            this.LastNametxt = new System.Windows.Forms.TextBox();
            this.UserNametxt = new System.Windows.Forms.TextBox();
            this.PassWordtxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(35, 138);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(103, 23);
            this.Save.TabIndex = 0;
            this.Save.Text = "SignUp";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.button1_Click);
            // 
            // Return
            // 
            this.Return.Location = new System.Drawing.Point(154, 138);
            this.Return.Name = "Return";
            this.Return.Size = new System.Drawing.Size(101, 23);
            this.Return.TabIndex = 1;
            this.Return.Text = "Return";
            this.Return.UseVisualStyleBackColor = true;
            this.Return.Click += new System.EventHandler(this.button2_Click);
            // 
            // Nametxt1
            // 
            this.Nametxt1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Nametxt1.Location = new System.Drawing.Point(106, 36);
            this.Nametxt1.Name = "Nametxt1";
            this.Nametxt1.Size = new System.Drawing.Size(149, 20);
            this.Nametxt1.TabIndex = 2;
            this.Nametxt1.TextChanged += new System.EventHandler(this.Txt1_TextChanged);
            // 
            // LastNametxt
            // 
            this.LastNametxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.LastNametxt.Location = new System.Drawing.Point(106, 62);
            this.LastNametxt.Name = "LastNametxt";
            this.LastNametxt.Size = new System.Drawing.Size(149, 20);
            this.LastNametxt.TabIndex = 3;
            this.LastNametxt.TextChanged += new System.EventHandler(this.LastNametxt_TextChanged);
            // 
            // UserNametxt
            // 
            this.UserNametxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.UserNametxt.Location = new System.Drawing.Point(106, 88);
            this.UserNametxt.Name = "UserNametxt";
            this.UserNametxt.Size = new System.Drawing.Size(149, 20);
            this.UserNametxt.TabIndex = 4;
            this.UserNametxt.TextChanged += new System.EventHandler(this.UserNametxt_TextChanged);
            // 
            // PassWordtxt
            // 
            this.PassWordtxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.PassWordtxt.Location = new System.Drawing.Point(106, 112);
            this.PassWordtxt.Name = "PassWordtxt";
            this.PassWordtxt.Size = new System.Drawing.Size(149, 20);
            this.PassWordtxt.TabIndex = 5;
            this.PassWordtxt.TextChanged += new System.EventHandler(this.PassWordtxt_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "LastName :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "UserName :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Password :";
            // 
            // UserRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 190);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PassWordtxt);
            this.Controls.Add(this.UserNametxt);
            this.Controls.Add(this.LastNametxt);
            this.Controls.Add(this.Nametxt1);
            this.Controls.Add(this.Return);
            this.Controls.Add(this.Save);
            this.Name = "UserRegister";
            this.Text = "Register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Return;
        private System.Windows.Forms.TextBox Nametxt1;
        private System.Windows.Forms.TextBox LastNametxt;
        private System.Windows.Forms.TextBox UserNametxt;
        private System.Windows.Forms.TextBox PassWordtxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}