﻿namespace MyProjectNorooz
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Serch = new System.Windows.Forms.Button();
            this.Searchtxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BookGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.BookGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Serch
            // 
            this.Serch.Location = new System.Drawing.Point(236, 24);
            this.Serch.Name = "Serch";
            this.Serch.Size = new System.Drawing.Size(109, 20);
            this.Serch.TabIndex = 0;
            this.Serch.Text = "Search";
            this.Serch.UseVisualStyleBackColor = true;
            this.Serch.Click += new System.EventHandler(this.Serch_Click);
            // 
            // Searchtxt
            // 
            this.Searchtxt.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Searchtxt.Location = new System.Drawing.Point(82, 24);
            this.Searchtxt.Name = "Searchtxt";
            this.Searchtxt.Size = new System.Drawing.Size(136, 20);
            this.Searchtxt.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search :";
            // 
            // BookGridView
            // 
            this.BookGridView.AllowUserToAddRows = false;
            this.BookGridView.AllowUserToDeleteRows = false;
            this.BookGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BookGridView.Location = new System.Drawing.Point(32, 63);
            this.BookGridView.Name = "BookGridView";
            this.BookGridView.ReadOnly = true;
            this.BookGridView.Size = new System.Drawing.Size(843, 341);
            this.BookGridView.TabIndex = 3;
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(910, 432);
            this.Controls.Add(this.BookGridView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Searchtxt);
            this.Controls.Add(this.Serch);
            this.Name = "Search";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BookGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Serch;
        private System.Windows.Forms.TextBox Searchtxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView BookGridView;
    }
}