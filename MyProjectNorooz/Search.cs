﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;


namespace MyProjectNorooz
{
	public partial class Search : Form
	{
		public Search()
		{
			InitializeComponent();
		}

		private void Serch_Click(object sender, EventArgs e)
		{
			List<string> AllBookInformations = new List<string>();
            if (File.Exists("F:\\SaveBook.txt"))
            {
                StreamReader SaveBook = new StreamReader("F:\\SaveBook.txt");
                while (!SaveBook.EndOfStream)
                {
                    AllBookInformations.Add(SaveBook.ReadLine());
                }
                SaveBook.Close();

            }
            List<string> SearchResultBookInformations = new List<string>();

			foreach (var Item in AllBookInformations)
			{
                if (Item.Split('/').ToList().Count() > 0)
                {
                    if (Item.Split('/')[0].Contains(Searchtxt.Text) || Item.Split('/')[1].Contains(Searchtxt.Text))
                    {
                        SearchResultBookInformations.Add(Item);
                    }
                }		
			}

			DataTable BookInf = new DataTable();
			BookInf.Columns.Add("Name", typeof(string));
			BookInf.Columns.Add("Auther", typeof(string));
			BookInf.Columns.Add("publisher", typeof(string));
			BookInf.Columns.Add("publishDate", typeof(string));
			BookInf.Columns.Add("Subject", typeof(string));
			BookInf.Columns.Add("Riview", typeof(string));
			BookInf.Columns.Add("Score", typeof(string));
			BookInf.Columns.Add("Summery", typeof(string));

            foreach (var Item in SearchResultBookInformations)
            {
                DataRow row = BookInf.NewRow();
                row["Name"] = Item.Split('/')[0];
                row["Auther"] = Item.Split('/')[1];
                row["Publisher"] = Item.Split('/')[2];
                row["PublishDate"] = Item.Split('/')[3];
                row["Subject"] = Item.Split('/')[4];
                row["Riview"] = Item.Split('/')[5];
                row["Score"] = Item.Split('/')[6];
                row["Summery"] = Item.Split('/')[7];
                BookInf.Rows.Add(row);
            }

            BookGridView.DataSource = BookInf;

		}

        private void Search_Load(object sender, EventArgs e)
        {


            DataTable BookInf = new DataTable();
            BookInf.Columns.Add("Name", typeof(string));
            BookInf.Columns.Add("Auther", typeof(string));
            BookInf.Columns.Add("publisher", typeof(string));
            BookInf.Columns.Add("publishDate", typeof(string));
            BookInf.Columns.Add("Subject", typeof(string));
            BookInf.Columns.Add("Riview", typeof(string));
            BookInf.Columns.Add("Score", typeof(string));
            BookInf.Columns.Add("Summery", typeof(string));

            List<string> AllBookInformations = new List<string>();
            if (File.Exists("F:\\SaveBook.txt"))
            {
                StreamReader SaveBook = new StreamReader("F:\\SaveBook.txt");
                while (!SaveBook.EndOfStream)
                {
                    AllBookInformations.Add(SaveBook.ReadLine());
                }
                SaveBook.Close();

            }

            foreach (var Item in AllBookInformations)
            {
                DataRow row = BookInf.NewRow();
                row["Name"] = Item.Split('/')[0];
                row["Auther"] = Item.Split('/')[1];
                row["Publisher"] = Item.Split('/')[2];
                row["PublishDate"] = Item.Split('/')[3];
                row["Subject"] = Item.Split('/')[4];
                row["Riview"] = Item.Split('/')[5];
                row["Score"] = Item.Split('/')[6];
                row["Summery"] = Item.Split('/')[7];
                BookInf.Rows.Add(row);
            }

            BookGridView.DataSource = BookInf;
        }
    }
}
